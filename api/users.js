const express = require('express');
const crypto = require('crypto');
const userDao = require('../databs/users');
const subjectDao = require('../databs/subjects');

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const router = express.Router();

// ---------------------crud------------------------------------

router.get('/:owner/subjects', (req, res) => {
  const { owner } = req.params;
  subjectDao.findSubjectByOwner(owner)
    .then((user) => (user ? res.json(user) : res.status(404).json({ message: `Subject owner with id ${owner} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding subject owner with id ${owner}: ${err.message}` }));
});

// ------------------------------------------------------------

router.post('/logindata', (request, response) => {
  const { username, password } = request.fields;

  userDao.selectUser(username)
    .then((result) => {
      if (result.userPassword !== undefined) {
        const expectedHash = result.userPassword.substring(0, hashSize * 2);
        const salt = Buffer.from(result.userPassword.substring(hashSize * 2), 'hex');
        const binaryHash = crypto.pbkdf2Sync(password, salt, iterations, hashSize, hashAlgorithm);
        const actualHash = binaryHash.toString('hex');

        if (actualHash === expectedHash) {
          request.session.username = username;
          request.session.role = result.userRole;
          subjectDao.selectFirstSubject()
            .then((subject) => response.redirect(`/api/assignments/:${subject[0].subjectCode}`))
            .catch((selecterr) => response.status(500).render('error', { message: `Selection unsuccessful: ${selecterr.message}`, clickmessage: 'kattintva a kezdőoldalra ugorhatsz.', username: request.session.username }));
        } else {
          response.render('signin', { message: 'Helytelen jelszó.', username });
        }
      } else {
        response.render('error', { message: 'Nem szerepel ezzel a névvel felhasználó az adatbázisban.', link: '/api/users/signup', clickmessage: 'kattintva a regisztrációhoz ugorhatsz.' });
      }
    })
    .catch((err) => response.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, clickmessage: 'kattintva a a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.post('/registerdata', (request, response) => {
  const {
    email, username, role, password, password2,
  } = request.fields;
  let roleName = 'student';

  userDao.selectUser(username)
    .then((result) => {
      if (result === undefined) {
        response.render('signup', {
          submitted: true, email, message: 'Ez a felhasználónév már foglalt.',
        });
      } else if (password !== password2) {
        response.render('signup', {
          submitted: true, email, username, message: 'A jelszók nem egyeznek meg.',
        });
      } else {
        if (role === 'Tanár') {
          roleName = 'admin';
        }
        const salt = crypto.randomBytes(saltSize);
        const hash = crypto.pbkdf2Sync(password, salt, iterations, hashSize, hashAlgorithm);
        const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');

        userDao.insertUser({
          email, username, roleName, hashWithSalt,
        })
          .then(response.render('signup', { submitted: true }))
          .catch((inserterr) => response.status(500).render('error', { message: `Insertion unsuccessful: ${inserterr.message}`, link: '/api/users/login', clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.' }));
      }
    })
    .catch((err) => response.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.post('/logout', (request, response) => {
  request.session.destroy((err) => {
    if (err) {
      response.status(500).send(`Session reset error: ${err.message}`);
    } else {
      response.redirect('/api/users/login');
    }
  });
});

router.get('/signup', (request, response) => {
  response.render('signup');
});

router.get('/login', (request, response) => {
  response.render('signin');
});

module.exports = router;
