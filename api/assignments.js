const express = require('express');
const assignmentDao = require('../databs/assignments');

const router = express.Router();

router.post('/submit_assignment', (request, response) => {
  assignmentDao.insertAssignment(request)
    .then(response.redirect(`/api/assignments/${request.fields.subjectcode}`))
    .catch((err) => response.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.get('/:subjectCode', (request, response) => {
  assignmentDao.selectAssignments(request.params.subjectCode)
    .then((assignments) => response.render('assignmentselection', { subjectCode: request.params.subjectCode, assignments, username: request.session.username }))
    .catch((err) => response.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.delete('/:assignmentCode', (request, response) => {
  assignmentDao.deleteAssignment(request.params.assignmentCode)
    .then(response.json([{ error: true }]))
    .catch(response.json([{ error: false }]));
});

module.exports = router;
