const express = require('express');
const bodyParser = require('body-parser');
const subjectDao = require('../databs/subjects');
const middlewareAdmin = require('../middleware/authadmin');
const validate = require('../middleware/validate');
const userDao = require('../databs/users');

const router = express.Router();
router.use(bodyParser.json());

// ---------------------crud------------------------------------

router.get(['/', '/main'], (request, response) => {
  subjectDao.selectSubjects()
    .then((subjects) => response.render('subjectselection', { subjects, username: request.session.username }))
    .catch((err) => response.status(500).json({ message: `Selection unsuccessful: ${err.message}` }));
});

router.get('/:subjectCode', (request, response) => {
  const { subjectCode } = request.params;
  subjectDao.findSubjectByCode(subjectCode)
    .then((subject) => (subject ? response.json(subject) : response.status(404).json({ message: `Subject with subjectCode ${subjectCode} not found.` })))
    .catch((err) => response.status(500).json({ message: `Error while finding subject with subjectCode ${subjectCode}: ${err.message}` }));
});

router.post('/', validate.hasProps(['owner', 'subjectCode', 'sDescription']), (request, response) => {
  userDao.userExists(request.fields.owner)
    .then((exists) => {
      if (!exists) {
        return response.status(400).json({ message: `User ${request.fields.owner} doesn't exists` });
      }
      return subjectDao.insertSubject(request.fields)
        .then((subject) => response.status(201).location(`${request.fullUrl}/${subject.subjectCode}`).json(subject))
        .catch((err) => response.status(500).json({ message: `Error while creating subject: ${err.message}` }));
    });
});

router.delete('/:subjectCode', (request, response) => {
  const { subjectCode } = request.params;
  subjectDao.deleteSubject(subjectCode)
    .then((rows) => (rows ? response.sendStatus(204) : response.status(404).json({ message: `Subject with subjectCode ${subjectCode} not found.` })))
    .catch((err) => response.status(500).json({ message: `Error while deleting subject with subjectCode ${subjectCode}: ${err.message}` }));
});

router.patch('/:subjectCode', (request, response) => {
  const { subjectCode } = request.params;
  subjectDao.updateSubject(subjectCode, request.fields)
    .then((rows) => (rows ? response.sendStatus(204) : response.status(404).json({ message: `Subject with subjectCode ${subjectCode} not found.` })))
    .catch((err) => response.status(500).json({ message: `Error while updating subject with subjectCode ${subjectCode}: ${err.message}` }));
});

// ---------------------------------------------------------------

router.get('/addsubject', (request, response) => {
  response.render('submitsubject', { username: request.session.username });
});

router.post('/submitsubject', middlewareAdmin, (request, response) => {
  subjectDao.insertSubject(request)
    .then(response.redirect('main'))
    .catch((err) => response.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.get('/:subjectCode/description', (request, response) => {
  const { subjectCode } = request.params;
  subjectDao.selectDescription(subjectCode)
    .then((description) => response.json(description))
    .catch((err) => response.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

router.post('/delete_subject', middlewareAdmin, validate.hasProps(['subjectCode']), (request, response) => {
  subjectDao.deleteSubject(request.fields.subjectCode)
    .then(response.redirect('/api/subjects/'))
    .catch((err) => response.status(500).render('error', { message: `Deletion unsuccessful: ${err.message}`, clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username }));
});

module.exports = router;
