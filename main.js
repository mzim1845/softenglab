const express = require('express');
const path = require('path');
const eformidable = require('express-formidable');
const fs = require('fs');
const session = require('express-session');
const hbs = require('hbs');
const apiRoutes = require('./api/main.js');
const errorMiddleware = require('./middleware/error');

const uploadDir = path.join(__dirname, '/static/uploads');
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

hbs.registerPartials(path.join(__dirname, '/views/partials/'));
hbs.registerHelper('ifEquals', function f(arg1, arg2, options) {
  return (arg1 === arg2) ? options.fn(this) : options.inverse(this);
});

const app = express();

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'static')));
app.use(eformidable({ uploadDir }));

app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));

app.use('/api', apiRoutes);
app.use(errorMiddleware);

app.listen(8080, () => {
  console.log('Server listening...');
});
