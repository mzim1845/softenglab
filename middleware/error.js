module.exports = (request, response) => {
  response.status(404).render('error', {
    message: 'A keresett végpont nem található.', link: '/api/users/login', clickmessage: 'kattintva a bejelentkezési oldalra ugorhatsz.', username: request.session.username,
  });
};
