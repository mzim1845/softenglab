const mysql = require('mysql');

const pool = mysql.createPool({
  database: 'subjectregister',
  host: 'localhost',
  port: 3306,
  user: 'subjectregister',
  password: 'ChGD6N2Y',
});

module.exports = (query, options = []) => new Promise((resolve, reject) => {
  pool.query(query, options, (error, results) => {
    if (error) {
      reject(new Error(`Error while running '${query}: ${error}'`));
    }
    resolve(results);
  });
});
