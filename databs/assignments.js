const fs = require('fs');
const path = require('path');
const execute = require('./config');

execute(`
CREATE TABLE IF NOT EXISTS assignments (
assignmentCode INT AUTO_INCREMENT,
subjectCode VARCHAR(12), 
aDescription VARCHAR(75), 
deadline DATE,
fileName VARCHAR(50),
PRIMARY KEY (assignmentCode),
FOREIGN KEY (subjectCode) REFERENCES subjects(subjectCode) ON DELETE CASCADE
);
`);

exports.selectAssignments = (subjectCode) => execute('SELECT assignments.subjectCode, assignmentCode, aDescription, deadline, fileName, subjects.owner FROM assignments INNER JOIN  subjects ON assignments.subjectCode=subjects.subjectCode WHERE assignments.subjectCode=?', [subjectCode]);

exports.selectDescription = (subjectCode) => execute('SELECT sDescription FROM subjects WHERE subjectCode=?', [subjectCode])
  .then((result) => ({ sDescription: result.sDescription }));

exports.insertAssignment = (assignment) => {
  const fileHandler = assignment.files.myfile;
  const newPath = path.join(__dirname, `/../static/uploads/${fileHandler.name}`);
  fs.renameSync(fileHandler.path, newPath);

  return execute('INSERT INTO assignments(subjectCode, aDescription, deadline, fileName) VALUES (?, ?, ?, ?)', [assignment.fields.subjectcode, assignment.fields.assignmentdescription, assignment.fields.deadline, fileHandler.name])
    .then((result) => result.affectedRows > 0);
};

exports.deleteAssignment = (assignmentCode) => execute('DELETE FROM assignments WHERE assignmentCode=?', [assignmentCode])
  .then((result) => result.affectedRows > 0);
