const path = require('path');
const mysql = require('mysql');
const fs = require('fs');

const pool = mysql.createPool({
  database: 'subjectregister',
  host: 'localhost',
  port: 3306,
  user: 'subjectregister',
  password: 'ChGD6N2Y',
});


pool.query(`CREATE TABLE IF NOT EXISTS users (
  userName varchar(25),
  email VARCHAR(50),
  userPassword VARCHAR(100),
  userRole VARCHAR(5))`, (errCreate) => {
  if (errCreate) {
    console.error(`Create table error: ${errCreate.message}`);
    process.exit(1);
  } else {
    console.log('Table users was created successfully');
  }
});

pool.query(`CREATE TABLE IF NOT EXISTS subjects (
      owner VARCHAR(25),
      subjectCode VARCHAR(12) NOT NULL PRIMARY KEY,
      sDescription VARCHAR(75))`, (errCreate) => {
  if (errCreate) {
    console.error(`Create table error: ${errCreate.message}`);
    process.exit(1);
  } else {
    console.log('Table subjects was created successfully');
  }
});

// Feladatok
pool.query(`CREATE TABLE IF NOT EXISTS assignments (
      assignmentCode INT AUTO_INCREMENT,
      subjectCode VARCHAR(12), 
      aDescription VARCHAR(75), 
      deadline DATE,
      fileName VARCHAR(50),
      PRIMARY KEY (assignmentCode),
      FOREIGN KEY (subjectCode) REFERENCES subjects(subjectCode) ON DELETE CASCADE)`, (errCreate) => {
  if (errCreate) {
    console.error(`Create table error: ${errCreate.message}`);
    process.exit(1);
  } else {
    console.log('Table assignments was created successfully');
  }
});

exports.selectSubjects = (callback) => {
  const query = 'SELECT * FROM subjects;';
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.selectFirstSubject = (callback) => {
  const query = 'SELECT subjectCode FROM subjects LIMIT 1;';
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.selectAssignments = (subjectCode, callback) => {
  const query = `SELECT assignments.subjectCode, assignmentCode, aDescription, deadline, fileName, subjects.owner 
                 FROM assignments
                INNER JOIN  subjects ON assignments.subjectCode=subjects.subjectCode
                WHERE assignments.subjectCode="${subjectCode}"`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.selectDescription = (request, callback) => {
  const query = `SELECT sDescription FROM subjects WHERE subjectCode="${request.fields.subjectCode}"`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.insertSubject = (request, callback) => {
  const query = `INSERT INTO subjects VALUES ("${request.session.username}", "${request.fields.subjectcode}", "${request.fields.subjectdescription}");`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.insertAssignment = (request, callback) => {
  const fileHandler = request.files.myfile;

  const newPath = path.join(__dirname, `/../static/uploads/${fileHandler.name}`);
  fs.renameSync(fileHandler.path, newPath);

  const query = `INSERT INTO assignments(subjectCode, aDescription, deadline, fileName) VALUES ("${request.fields.subjectcode}", "${request.fields.assignmentdescription}", "${request.fields.deadline}", "${fileHandler.name}");`;
  pool.query(query, callback);
  console.log(`Executing query ${query}`);
};

exports.insertUser = (request, callback) => {
  const query = `INSERT INTO users VALUES ("${request.username}", "${request.email}", "${request.hashWithSalt}", "${request.roleName}");`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.selectUser = (username, callback) => {
  const query = `SELECT userPassword, userRole FROM users WHERE userName="${username}";`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.deleteSubject = (request, callback) => {
  const query = `DELETE FROM subjects WHERE subjectCode="${request.fields.subjectcode}";`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};

exports.deleteAssignment = (request, callback) => {
  const query = `DELETE FROM assignments WHERE assignmentCode="${request.fields.assignmentCode}";`;
  console.log(`Executing query ${query}`);
  pool.query(query, callback);
};
