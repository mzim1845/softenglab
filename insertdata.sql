use subjectregister;

-- DROP TABLE IF EXISTS assignments;
-- SELECT * FROM subjects;
-- DROP TABLE users;
-- SELECT * FROM users;
-- SELECT * FROM assignments;

CREATE TABLE IF NOT EXISTS users (
	userName varchar(25),
	email VARCHAR(50),
	userPassword VARCHAR(100),
	userRole VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS subjects (
	owner VARCHAR(25),
	subjectCode VARCHAR(12) NOT NULL PRIMARY KEY,
	sDescription VARCHAR(75)
);
      
CREATE TABLE IF NOT EXISTS assignments (
	assignmentCode INT AUTO_INCREMENT,
	subjectCode VARCHAR(12), 
	aDescription VARCHAR(75), 
	deadline DATE,
	fileName VARCHAR(50),
	PRIMARY KEY (assignmentCode),
	FOREIGN KEY (subjectCode) REFERENCES subjects(subjectCode) ON DELETE CASCADE
);

